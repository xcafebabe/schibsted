const SIZES = [200, 304, 400, 504, 600, 704]

const getRandomSize = () => SIZES[Math.floor(Math.random() * SIZES.length)]

/**
 * Generate a random image url
 * @param {*} width Image Width. By default size random selected from SIZE
 * @param {*} height Image Height. By default size random selected from SIZE
 */
const getPicsumUrl = (width = getRandomSize(), height = getRandomSize()) =>
  `https://picsum.photos/${width}/${height}/?random`

export { getRandomSize, getPicsumUrl }
