/**
 * Dont like Event Hell!
 *
 * Using a simple observer pattern
 */

function Observer() {
  /* Initialize list of observers */
  const _observerService = {}

  _observerService.observers = {}

  _observerService.connect = (id, callback) => {
    console.debug('Attach to', id)
    if (!id) {
      throw new Error('You must provide an identifier subscription')
    }

    if (!_observerService.observers[id]) {
      _observerService.observers[id] = []
      console.log(`New ${id} subscription`)
    }

    _observerService.observers[id].push(callback)
  }
  /**
   * @param {string} id name of the subscription
   * @description removes subscription from the observer object
   */
  _observerService.disconnect = function(id) {
    if (id in _observerService.observers) {
      delete _observerService.observers[id]
    }
  }

  /**
   * @param {string} id name of the subscription
   * @param {string|object|array|number} parameters pass whatever your listener is expecting
   * @description notifies all obbservers of a specific subscription
   */
  _observerService.notify = function(id, parameters) {
    console.debug('Publish', id, parameters)
    //console.debug(_observerService.observers)
    if (_observerService.observers[id]) {
      _observerService.observers[id].forEach(callback => callback(parameters))
    }
  }

  return _observerService
}

const ObserverService = new Observer()
ObserverService.getNewInstance = Observer

export { ObserverService }
