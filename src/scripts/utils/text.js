const getHash = (element = '') => {
  let hash = 0
  let i
  let chr
  const length = element.length
  if (length > 0) {
    for (i = 0; i < length; i++) {
      chr = element.charCodeAt(i)
      hash = (hash << 5) - hash + chr
      hash |= 0 // Convert to 32bit integer
    }
  }
  return Math.abs(hash)
}

export { getHash }
