import { ObserverService } from '../utils/observer'

const tagName = 'sbd-item'
const template = document.createElement('template')
template.innerHTML = `    
  <div class="column is-half has-text-centered sbd-loader">      
    <a class="button is-loading">Loading</a>       
  </div>
`

const cardTemplate = item => {
  // Random selection for New label
  const newItem =
    Math.floor(Math.random() * 5) === 2
      ? `<div class="labels">
          <span class="tag is-dark">NEW</span>
        </div>`
      : ''
  return `<div class="card">
      <div class="card-image">
        <figure class="image is-4by3">
          <img src="${item.imageUrl}" alt="${item.title}">
        </figure>
        ${newItem}
      </div>
      <div class="card-content sbd">
        <div class="content">
          <div class="provider">
            <a class="provider-link" href="#" target="_blank">
              <img class="provider-image" src="${item.provider.imageUrl}">
            </a>
          </div>
          <div class="info">
            <div class="bumpdate">
              <span class="timeago">${item.timeago}</span>
            </div>
            <div class="meta">
              <div class="meta-link">${item.title}</div>
              <div class="meta-price">${item.price}</div>
              <div class="meta-tags">
                <span class="meta-tag">${item.tag1}</span>
                <span class="meta-tag">${item.tag2}</span>
              </div>
            </div>            
          </div>
        </div>
      </div>
    </div>`
}

class SbdItem extends HTMLElement {
  constructor() {
    super()
    this.render = this.render.bind(this)
  }

  get name() {
    return this.getAttribute('data-name')
  }
  set name(name = 'random') {
    return this.setAttribute('data-name', name)
  }

  get click() {
    return this.getAttribute('data-click')
  }
  set click(clickName = 'random') {
    return this.setAttribute('data-click', name)
  }

  connectedCallback() {
    this.appendChild(template.content.cloneNode(true))
    const self = this
    ObserverService.connect(
      `sbd-item-${this.name}`,
      params => {
        self.render(params)
      }
    )
  }

  disconnectedCallback() {
    ObserverService.disconnect(`sbd-item-${this.name}`)
  }

  render(item) {
    this.innerHTML = cardTemplate(item)
  }
}

const register = () => customElements.define(tagName, SbdItem)
window.WebComponents ? window.WebComponents.waitFor(register) : register()
console.log('registerd sbd-item')
