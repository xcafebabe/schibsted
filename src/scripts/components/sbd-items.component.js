import './sbd-item.component'
import * as api from '../api'
import { ObserverService } from '../utils/observer'
import { getHash } from '../utils/text'

const tagName = 'sbd-items'
const template = document.createElement('template')
template.innerHTML = `   
  <div class="column is-half has-text-centered sbd-loader">      
    <a class="button is-info">Click me</a> 
  </div>
`

class SbdItems extends HTMLElement {
  constructor() {
    super()
    // this.attachShadow({ mode: 'open' })
    // this.shadowRoot.appendChild(template.content.cloneNode(true))
    // I didn't want to use Shadow Dom because I wanted to use a Css Framework to save me hours of work with stylings
  }

  get limit() {
    return this.getAttribute('data-limit')
  }

  set limit(newLimit = 10) {
    this.setAttribute('data-limit', newLimit)
  }

  connectedCallback() {
    // Loader Component
    const self = this

    this.appendChild(template.content.cloneNode(true))
    const buttonLoader = this.querySelector('.sbd-loader .button')
    buttonLoader.addEventListener('click', ev => {
      if (!buttonLoader.classList.contains('is-loading')) {
        buttonLoader.classList.add('is-loading')
        api.items
          .find()
          .then(items => {
            console.log(`Scrapped ${items.length} elements`)
            buttonLoader.innerHTML = 'Loaded :)'
            buttonLoader.classList.remove('is-loading')
            buttonLoader.parentNode.style.opacity = '0'
            setTimeout(() => {
              buttonLoader.parentNode.parentNode.removeChild(
                buttonLoader.parentNode
              )
              this.classList.remove('is-centered')
              self.render(items.slice(0, Math.min(items.length, this.limit)))
            }, 750)
          })
          .catch(err => {
            buttonLoader.classList.remove('is-loading')
            buttonLoader.classList.add('is-danger')
            buttonLoader.innerHTML = 'Failure :('
            console.error('Problem with Apify', err)
          })
      }
    })
  }

  render(items = []) {
    console.log(`rendering ${items.length} elements`)
    items.forEach(item => {
      const hashId = getHash(`${item.title}-${item.price}`)
      const newElement = document.createElement('sbd-item')
      newElement.setAttribute('data-name', hashId)
      newElement.classList.add('column', 'is-4')
      this.appendChild(newElement)
      ObserverService.notify(`sbd-item-${hashId}`, item)
    })
  }
}

const register = () => customElements.define(tagName, SbdItems)
window.WebComponents ? window.WebComponents.waitFor(register) : register()
console.log('registered sbd-items')
