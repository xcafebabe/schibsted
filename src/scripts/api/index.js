import { getPicsumUrl } from '../utils/images'

const DUMMY_API_URL_FIND = `https://api.apify.com/v1/3Z96YGuWfyeodJF53/crawlers/JkPsSxB3GgBiu6ZEC/lastExec/results?token=i54xuGGvhqHaxvziTv7JMJgAx`
const DUMMY_API_URL_REFRESH = `https://api.apify.com/v1/3Z96YGuWfyeodJF53/crawlers/JkPsSxB3GgBiu6ZEC/execute?token=jGANiZtKXrLPHujQgCQa7BBFH`

const items = {
  /**
   * item {
   *  "title": "Dúplex en Ramblas, Centre - Colomeres - Rambles",
   *  "price": "475.000 €",
   *  "tag1": "3 habs.",
   *  "tag2": "138 m²",
   *  "timeago": "ahora"
   *  "imageUrl": "random-url-generation"
   *  "provider": {
   *    "imageUrl": "random-url-generation"
   *  }
   * },
   *@return {Promise} Data item list with scrapped data from Fotocasa using Apify service

   */
  find: options =>
    fetch(DUMMY_API_URL_FIND).then(response =>
      response.json().then(data =>
        data[0].pageFunctionResult.map(item => ({
          ...item,
          imageUrl: getPicsumUrl(),
          provider: {
            imageUrl: getPicsumUrl(100, 52),
          },
        }))
      )
    ),
  /**
   * Ability to run Apify crawler
   */
  refresh: options => fetch(DUMMY_API_URL_REFRESH),
}

export { items }
