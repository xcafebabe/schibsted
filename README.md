# Schibsted Demo

Author: Luis Toubes <luis@toub.es>

## Installation

Installation is done using the `yarn install` command (you can also use `npm`)

```
yarn install
```

## Quick Start

Start the server

```
yarn start
```

Now you can visit http://localhost:1234 to view this demo

You should see a blue button `Click me`. Click on it and 10 items will be loaded on the page using Web Components specifications.

## Features

- A crawler from Apify.com scrapes randomly several cities from Fotocasa.es.
- To avoid surprises, the crawler only updates scrapped data if this [request](https://api.apify.com/v1/3Z96YGuWfyeodJF53/crawlers/JkPsSxB3GgBiu6ZEC/execute?token=jGANiZtKXrLPHujQgCQa7BBFH) is manually triggered.
- ParcelJS is used to bundle this demo.
- In order to be able to focus on the Web Component test I have used a css framework, [Bulma](https://bulma.io/).
- I have prepared two custom elements, `sbd-items` and `sbd-item`.
- You can limit the number of displayed items using `data-limit`.
- Im designed a small service for state management based on the Observer pattern.
